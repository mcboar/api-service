import os
from pathlib import Path

import numpy as np
import onnxruntime as rt
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.responses import JSONResponse

app = FastAPI()
load_dotenv()


@app.post("/predict")
def predict(age: int = 0, sex: int = 0, cp: int = 0, trestbps: int = 0, chol: int = 0,
            fbs: int = 0, restecg: int = 0, thalach: int = 0, exeang: int = 0,
            oldpeak: int = 0, slope: int = 0, ca: int = 0,
            thal: int = 0) -> JSONResponse:
    """
    Predicts probabilities with model
    :param age: age feature
    :param sex: sex feature
    :param cp: chest pain type (4 values) feature
    :param trestbps: resting blood pressure feature
    :param chol: serum cholestoral in mg/dl feature
    :param fbs: fasting blood sugar > 120 mg/dl feature
    :param restecg: resting electrocardiographic results (values 0,1,2) feature
    :param thalach: maximum heart rate achieved feature
    :param exeang: exercise induced angina feature
    :param oldpeak: ST depression induced by exercise relative to rest feature
    :param slope: the slope of the peak exercise ST segment feature
    :param ca: number of major vessels (0-3) colored by flourosopy feature
    :returns: JSONResponse
    """
    vector = [age, sex, cp, trestbps, chol, fbs, restecg, thalach, exeang, oldpeak,
              slope, ca]

    sess = rt.InferenceSession(Path(os.getenv("MODEL_PATH")),
                               providers=["CPUExecutionProvider"])

    input_name = sess.get_inputs()[0].name
    output_name = sess.get_outputs()[0].name

    prediction = sess.run(
        [output_name],
        {input_name: np.array([vector]).astype(np.float32)}
    )[0][0]

    if prediction == 0:
        return JSONResponse(
            status_code=200,
            content={"Verdict": "Normal"}
        )

    if prediction == 1:
        return JSONResponse(
            status_code=200,
            content={"Verdict": "Fixed defect"}
        )

    if prediction == 2:
        return JSONResponse(
            status_code=200,
            content={"Verdict": "Reversable defect"}
        )
