FROM python:3.8.0

ENV MODEL_PATH /service/models/model.onnx

WORKDIR /service

COPY src/ /service/src/

RUN apt update && apt install -y --no-install-recommends make build-essential libffi-dev libpq-dev musl-dev && \
    pip3 install --upgrade pip && \
    pip3 install poetry==1.5.1

COPY pyproject.toml poetry.lock /service/
RUN poetry install

COPY models/ /service/models/

WORKDIR /service/src

CMD poetry run uvicorn service:app --host 127.0.0.1 --port 80
